###EMPLOYEE CRUD
This appliation provides the following basic employee management functionalities:
* creating
* getting all, 
* getting a specific employee
* updating a specific employee and 
* deleting Employee

The urls for this can be found in the Employee controller.

##HOW TO START UP THE APPLICATION
The application was written with Java spring-boot.
The start up the application, run the command:

mvn clean install
mvn spring-boot:run

An embedded db was used (H2) so no need to configure DB.

After which you can execute all rest actions.
Eg to create the employee
url: localhost:8080/employees
request json:
{
    "employeeId":"758978783",
    "firstName":"Jerry",
    "lastName":"Peter",
    "age":65,
    "joinDate":"2000-12-01"

}