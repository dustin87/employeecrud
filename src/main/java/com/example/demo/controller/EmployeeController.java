package com.example.demo.controller;

import com.example.demo.dto.CreateEmployeeDto;
import com.example.demo.dto.UpdateEmployeeDto;
import com.example.demo.exception.NotFoundException;
import com.example.demo.model.Employee;
import com.example.demo.repo.EmployeeDao;
import com.example.demo.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by David on 30 Apr, 2021
 **/
@RestController
@AllArgsConstructor
@CrossOrigin
@RequestMapping("employees")
public class EmployeeController {

    private EmployeeService employeeService;
    private EmployeeDao employeeDao;

    @PostMapping
    public ResponseEntity<?> createEmployee(@RequestBody @Valid CreateEmployeeDto dto) {
        Employee employee = employeeService.create(dto);

        return ResponseEntity.ok(employee);
    }



    @GetMapping
    public ResponseEntity<?> getAllEmployees() {

        return ResponseEntity.ok(employeeDao.findAll());
    }

    @GetMapping("/{employeeId}")
    public ResponseEntity<?> getOneEmployee(@PathVariable String employeeId) {
        Employee emp = employeeDao.findByEmployeeId(employeeId);
        if (emp == null)
            throw new NotFoundException("Employee not found");

        return ResponseEntity.ok(emp);
    }

    @PutMapping
    public ResponseEntity<?> updateOneEmployee(@RequestBody @Valid UpdateEmployeeDto dto) {
        return ResponseEntity.ok(employeeService.update(dto));
    }

    @DeleteMapping("/{employeeId}")
    public ResponseEntity<?> deleteOneEmployee(@PathVariable String employeeId) {

        Employee emp = employeeDao.findByEmployeeId(employeeId);
        if (emp == null)
            throw new NotFoundException("Employee not found");

        employeeDao.delete(emp);
        return ResponseEntity.ok(null);
    }
}
