package com.example.demo.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by David on 30 Apr, 2021
 **/
@Data
@Entity
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO )
    private Long id;

    @Column(unique = true)
    private String employeeId;

    private String firstName;

    private String lastName;

    private Integer age;

    private LocalDate joinDate;

    @CreationTimestamp
    private LocalDateTime createdAt;
}
