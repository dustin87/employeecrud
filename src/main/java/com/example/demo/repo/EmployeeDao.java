package com.example.demo.repo;

import com.example.demo.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by David on 30 Apr, 2021
 **/
public interface EmployeeDao extends JpaRepository<Employee, Long> {
    Boolean existsByEmployeeId(String employeeId);

    Employee findByEmployeeId(String employeeId);
}
