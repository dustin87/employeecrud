package com.example.demo.service;

import com.example.demo.dto.CreateEmployeeDto;
import com.example.demo.dto.UpdateEmployeeDto;
import com.example.demo.model.Employee;

/**
 * Created by David on 30 Apr, 2021
 **/
public interface EmployeeService {
    Employee create(CreateEmployeeDto dto);
    Employee update(UpdateEmployeeDto dto);
}
