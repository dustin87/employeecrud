package com.example.demo.service.impl;

import com.example.demo.dto.CreateEmployeeDto;
import com.example.demo.dto.UpdateEmployeeDto;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.model.Employee;
import com.example.demo.repo.EmployeeDao;
import com.example.demo.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by David on 30 Apr, 2021
 **/
@Service
@AllArgsConstructor
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    private EmployeeDao employeeDao;
    private ModelMapper modelMapper;

    @Override
    public Employee create(CreateEmployeeDto dto) {

        if (employeeDao.existsByEmployeeId(dto.getEmployeeId())) {
            throw new BadRequestException("Employee id already exists");
        }

        Employee employee = modelMapper.map(dto, Employee.class);

        return employeeDao.save(employee);
    }

    @Override
    public Employee update(UpdateEmployeeDto dto) {

        Employee emp = employeeDao.findByEmployeeId(dto.getEmployeeId());
        if (emp == null)
            throw new NotFoundException("Employee not found");

        emp.setFirstName(dto.getFirstName());
        emp.setLastName(dto.getLastName());


        return employeeDao.save(emp);
    }
}
