package com.example.demo.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * Created by David on 01 May, 2021
 **/
@Data
public class UpdateEmployeeDto {
    @NotEmpty(message = "Employee id is required")
    private String employeeId;

    @NotEmpty(message = "Employee first name is required")
    private String firstName;

    @NotEmpty(message = "Employee last name is required")
    private String lastName;
}
