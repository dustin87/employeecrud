package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by David on 30 Apr, 2021
 **/
@Data
public class CreateEmployeeDto {

    @NotEmpty(message = "Employee id is required")
    private String employeeId;

    @NotEmpty(message = "Employee first name is required")
    private String firstName;

    @NotEmpty(message = "Employee last name is required")
    private String lastName;

    @NotNull(message = "Employee age is required")
    private Integer age;

    @NotNull(message = "Join date is required")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate joinDate;
}
